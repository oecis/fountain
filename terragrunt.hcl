locals {
  env    = read_terragrunt_config("${get_repo_root()}/environments/${get_env("ENVIRONMENT")}.hcl")
  expose = true
}

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket                      = local.env.locals.s3.bucket
    region                      = local.env.locals.s3.region
    endpoint                    = local.env.locals.s3.endpoint
    profile                     = local.env.locals.s3.profile
    key                         = "${path_relative_to_include()}/terraform.tfstate"
    force_path_style            = true
    skip_credentials_validation = true
    disable_bucket_update       = true
    skip_bucket_versioning      = true
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"

  contents = <<EOF
terraform {
  required_providers {
    kubectl = {
      source = "alekc/kubectl"
      version = "2.0.4"
    }

    vault = {
      source  = "hashicorp/vault"
      version  = "4.2.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.35.0"
    }

    helm = {
      source = "hashicorp/helm"
      version = "2.16.1"
    }

    template = {
      source = "hashicorp/template"
      version = "2.2.0"
    }

    http = {
      source = "hashicorp/http"
      version = "3.4.2"
    }

    authentik = {
      source = "goauthentik/authentik"
      version = "2024.12.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "${local.env.locals.k8s_context}"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
    config_context = "${local.env.locals.k8s_context}"
  }
}

provider "kubectl" {
  config_path    = "~/.kube/config"
  config_context = "${local.env.locals.k8s_context}"
}

provider "authentik" {
  url = "https://authentik.${local.env.locals.domain}"
}

provider "random" {
}

provider "template" {
}

provider "vault" {
  address = "https://vault.${local.env.locals.domain}"
}
EOF
}
