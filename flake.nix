{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            allowUnfree = true;
          };
          overlays = [
            (final: prev: {
              terragrunt = prev.terragrunt.overrideAttrs (old: rec {
                version = "0.67.5";
                src = prev.fetchFromGitHub {
                  owner = "gruntwork-io";
                  repo = "terragrunt";
                  rev = "refs/tags/v${version}";
                  hash = "sha256-uzAVMgnC65puaJ9zt35Qpzj6mnCa3xQFCaUKDlBgTZA=";
                };
              });
            })
          ];
        };
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.opentofu
            pkgs.terragrunt
            pkgs.terraform-ls
            pkgs.kubectl
            pkgs.tflint
            pkgs.kubernetes-helm
            pkgs.pre-commit
            pkgs.vault-bin
            pkgs.awscli
          ];
        };
      }
    );
}
