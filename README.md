# The fountain of oecis

# Development

## Dependencies

* opentofu
* terragrunt >=0.52.3
* kubectl
* tflint
* helm
* pre-commit

This project uses direnv with nix. So when using direnv, everything will be automatically installed and loaded.
If you are not using direnv, make sure to install everything manually.

# Deployment

## Environments

Fountain supports configuring the setup according to different environments. An example environment file can be found in `environments/dev.hcl` which works with [reservoir](https://gitlab.com/oecis/reservoir) without any further configuration.
You can create your own environment files (e.g. `prod.hcl` in the `environments/` directory) and than reference it by setting the file name (without the file type suffix) in the `ENVIRONMENT` variable. Example for development:

```shell
$ export ENVIRONMENT=dev
```

Before executing any command, make sure that you have that set the `ENVIRONMENT` variable to the environment you want to deploy to.

## State Backend

For tofu you need a state backend where it can store the state. By default the S3 compatible minio is supported.
Before deploying you need to make sure that the state backend is correctly configured (e.g. by setting the credentials in `~/.aws/credentials`) and also set in your environment configuration.


## First time or after changes to the root `terragrunt.hcl`

If you want to deploy the first time, you should run `terragrunt run-all init -upgrade -reconfigure` in the stacks folder. This will initialize every stack in the stacks folder with the required files.

## Deploy

For example to deploy the ory stack:

```
$ cd stacks/ory && terragrunt run-all apply
...
$ cd ../..
```

To deploy only mirage:

```
$ cd stacks/oecis/mirage && terragrunt run-all apply
...
$ cd ../../..
```

# TODOs:

* the secret njalla-token is needed before deploying cert-manager. It is not automatically created!
