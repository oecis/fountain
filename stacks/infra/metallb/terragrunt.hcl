include "root" {
  path   = find_in_parent_folders()
  expose = true
}

locals {
  # bring environment configuration into scope
  env    = include.root.locals.env.locals
  expose = true
}

terraform {
  source = "${get_path_to_repo_root()}/modules//metallb"
}

inputs = {
  addresses = [
    # Assume a single IP per node
    "${local.env.host.ipv4}/32",
    "${local.env.host.ipv6}/128"
  ]
}
