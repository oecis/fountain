variable "domain" {
  type        = string
  description = "the base domain, from which searxng will derive all subdomains"
}

data "authentik_flow" "default-invalidation-flow" {
  slug = "default-provider-invalidation-flow"
}

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

resource "authentik_provider_proxy" "this" {
  name                  = "searxng"
  mode                  = "forward_single"
  external_host         = "https://search.${var.domain}"
  authorization_flow    = data.authentik_flow.default-authorization-flow.id
  invalidation_flow     = data.authentik_flow.default-invalidation-flow.id
  skip_path_regex       = "^/opensearch.xml.*"
  access_token_validity = "weeks=12"
}

resource "authentik_application" "this" {
  name              = "searxng"
  slug              = "searxng"
  protocol_provider = authentik_provider_proxy.this.id
}


resource "helm_release" "searxng" {
  name       = "searxng"
  repository = "https://charts.searxng.org"
  chart      = "searxng"

  values = [
    yamlencode({
      image = {
        repository = "searxng/searxng"
        tag        = "2025.1.6-6dab7fe78"
      }
      env = {
        TZ            = "Europe/Berlin"
        INSTANCE_NAME = "search.${var.domain}"
        BASE_URL      = "https://search.${var.domain}"
        AUTOCOMPLETE  = "false"
        # Our cluster uses IPv6 by default so searxng must also bind to an IPv6 address
        BIND_ADDRESS = "[::]:8080"
      }
      searxng = {
        config = {
          use_default_settings = {
            engines = {
              remove = [
                # TODO: disabled because of many errors, maybe they were fixed => testing required
                "soundcloud",
                "wikidata",
                "flickr",
                "deviantart",
                "genius",
                "gentoo",
              ]
            }
          },
          general = {
            # TODO: duplicated already in environment?
            instance_name = "search.${var.domain}"
          }
          outgoing = {
            request_timeout     = 5.0
            max_request_timeout = 10.0
            pool_maxsize        = 20
            pool_connections    = 100
          }
        }
      }
      ingress = {
        main = {
          enabled   = true
          className = "traefik"
          annotations = {
            "gethomepage.dev/enabled"                          = "true"
            "gethomepage.dev/name"                             = "SearXNG"
            "gethomepage.dev/description"                      = " SearXNG is a free internet metasearch engine which aggregates results from various search services and databases. Users are neither tracked nor profiled"
            "gethomepage.dev/group"                            = var.domain
            "traefik.ingress.kubernetes.io/router.middlewares" = "default-authentik-forwardauth@kubernetescrd"

          }
          hosts = [
            {
              host = "search.${var.domain}"
              paths = [
                {
                  path     = "/"
                  pathType = "ImplementationSpecific"
                }
              ]
            }
          ]
        }
      }
      redis = {
        # deploy redis alongside for caching
        enabled = true
      }
    })
  ]
}
