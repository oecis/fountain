resource "helm_release" "teamspeak3" {
  name        = "teamspeak3"
  repository  = "https://charts.oecis.io"
  chart       = "teamspeak3"
  version     = "1.0.0"
  max_history = 2

  values = [
    yamlencode({
      persistence = {
        enabled      = true
        storageClass = "local-path"
      }

    })
  ]
}

resource "kubectl_manifest" "teamspeak3-ingress-route-udp-voice" {
  yaml_body = yamlencode({
    apiVersion = "traefik.io/v1alpha1"
    kind       = "IngressRouteUDP"
    metadata = {
      name = "teamspeak3-voice"
    }
    spec = {
      entryPoints = ["ts3udp-voice"]
      routes = [{
        services = [{
          name = "teamspeak3-voice"
          port = 9987
        }]
      }]
    }
  })
}

resource "kubectl_manifest" "teamspeak3-ingress-route-tcp-file" {
  yaml_body = yamlencode({
    apiVersion = "traefik.io/v1alpha1"
    kind       = "IngressRouteTCP"
    metadata = {
      name = "teamspeak3-file"
    }
    spec = {
      entryPoints = ["ts3tcp-file"]
      routes = [{
        match = "HostSNI(`*`)"
        services = [
          {
            name = "teamspeak3-file"
            port = 30033
          }
        ]
      }]
    }
  })
}

resource "kubectl_manifest" "teamspeak3-ingress-route-tcp-query" {
  yaml_body = yamlencode({
    apiVersion = "traefik.io/v1alpha1"
    kind       = "IngressRouteTCP"
    metadata = {
      name = "teamspeak3-query"
    }
    spec = {
      entryPoints = ["ts3tcp-query"]
      routes = [{
        match = "HostSNI(`*`)"
        services = [
          {
            name = "teamspeak3-query"
            port = 10011
          }
        ]
      }]
    }
  })
}
