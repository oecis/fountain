resource "helm_release" "penpot-app" {
  name              = "penpot-app"
  repository        = "https://charts.codechem.com"
  chart             = "penpot"
  dependency_update = true

  values = [
    file("${path.module}/1-penpot-app-values.yaml")
  ]
  depends_on = [
    kubectl_manifest.hydra_penpot_oauth2_client
  ]
}

resource "kubectl_manifest" "hydra_penpot_oauth2_client" {
  yaml_body = file("${path.module}/manifests/penpot.yaml")
}

resource "kubernetes_secret" "penpot_db_credentials_secret" {
  metadata {
    name = "penpot-db-credentials"
  }

  data = {
    replication-password = data.vault_generic_secret.penpot_db_credentials.data["replication"]
    admin-password       = data.vault_generic_secret.penpot_db_credentials.data["admin"]
    user-password        = data.vault_generic_secret.penpot_db_credentials.data["user"]
  }
}

data "vault_generic_secret" "penpot_db_credentials" {
  path = "kv/services/penpot/db"
}
