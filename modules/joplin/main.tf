resource "kubernetes_secret" "joplin_db_credentials_secret" {
  metadata {
    name = "joplin-db-credentials"
  }

  data = {
    replication-password = data.vault_generic_secret.joplin_db_credentials.data["replication-password"]
    admin-password       = data.vault_generic_secret.joplin_db_credentials.data["admin-password"]
    user-password        = data.vault_generic_secret.joplin_db_credentials.data["user-password"]
  }
}

data "vault_generic_secret" "joplin_db_credentials" {
  path = "kv/services/joplin/db"
}

resource "helm_release" "joplin" {
  name       = "joplin"
  chart      = "joplin"
  repository = "https://charts.oecis.io"
  values = [yamlencode({
    ingress = {
      enabled = true
      hosts = [{
        host = "joplin.oecis.io"
        paths = [{
          path     = "/"
          pathType = "ImplementationSpecific"
        }]
      }]
    }
    postgresql = {
      enabled      = true
      architecture = "standalone"
      auth = {
        username       = "joplin"
        database       = "joplin"
        existingSecret = kubernetes_secret.joplin_db_credentials_secret.metadata[0].name
        secretKeys = {
          adminPasswordKey       = "admin-password"
          userPasswordKey        = "user-password"
          replicationPasswordKey = "replication-password"
        }
      }
    }
    joplin = {
      tz      = "Europe/Berlin"
      baseUrl = "https://joplin.oecis.io"
      db = {
        type           = "pg"
        host           = "joplin-postgresql"
        database       = "joplin"
        user           = "joplin"
        existingSecret = kubernetes_secret.joplin_db_credentials_secret.metadata[0].name
        secretKeys = {
          passwordKey = "user-password"
        }

      }
    }
  })]
}
