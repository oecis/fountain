resource "helm_release" "njalla-webhook" {
  name       = "njalla-webhook"
  chart      = "cert-manager-webhook-njalla"
  repository = "https://charts.oecis.io"

  namespace = "cert-manager"
  version   = "0.1.2"

  depends_on = [
    resource.kubectl_manifest.njalla-dns-issuer,
    resource.kubectl_manifest.njalla-webhook-secrets-reader-role,
    resource.kubectl_manifest.njalla-webhook-role-binding
  ]
}

resource "kubectl_manifest" "njalla-dns-issuer" {
  yaml_body = file("${path.module}/manifests/njalla-acme-webhook-issuer.yaml")
}

resource "kubectl_manifest" "njalla-webhook-secrets-reader-role" {
  yaml_body = file("${path.module}/manifests/njalla-webhook-secrets-reader-role.yaml")
}

resource "kubectl_manifest" "njalla-webhook-role-binding" {
  yaml_body = file("${path.module}/manifests/njalla-webhook-role-binding.yaml")
}
