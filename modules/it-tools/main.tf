variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}


data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

data "authentik_flow" "default-invalidation-flow" {
  slug = "default-provider-invalidation-flow"
}

resource "authentik_provider_proxy" "this" {
  name                  = "it-tools"
  mode                  = "forward_single"
  external_host         = "https://tools.${var.domain}"
  authorization_flow    = data.authentik_flow.default-authorization-flow.id
  invalidation_flow     = data.authentik_flow.default-invalidation-flow.id
  access_token_validity = "days=30"
}

resource "authentik_application" "this" {
  name              = "it-tools"
  slug              = "it-tools"
  protocol_provider = authentik_provider_proxy.this.id
}

resource "kubernetes_config_map" "nginx_config" {
  metadata {
    name = "it-tools-nginx-config"
  }

  # Make nginx listen on IPv6...
  # nginx config taken from it-tools repo: https://github.com/CorentinTh/it-tools/blob/main/nginx.conf
  data = {
    "default.conf" = <<NGINX
server {
    listen [::]:80;
    listen 80;
    server_name localhost;
    root /usr/share/nginx/html;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }
}
NGINX
  }
}

resource "helm_release" "this" {
  name        = "it-tools"
  chart       = "it-tools"
  repository  = "https://charts.oecis.io"
  max_history = 2
  version     = "0.2.0"

  values = [yamlencode({
    # Make nginx listen on IPv6...
    volumeMounts = [
      {
        name = "nginx-config"
        # Here the original nginx.conf is mounted in the Dockerfile, and we just override it by mounting our patched version
        mountPath = "/etc/nginx/conf.d/default.conf"
        subPath   = "default.conf"
      }
    ]
    volumes = [
      {
        name = "nginx-config"
        configMap = {
          name = kubernetes_config_map.nginx_config.metadata[0].name
        }
      }
    ]
    ingress = {
      enabled = true
      annotations = {
        "gethomepage.dev/enabled"                          = "true"
        "gethomepage.dev/name"                             = "IT-Tools"
        "gethomepage.dev/description"                      = "Collection of handy online tools for developers, with great UX. "
        "gethomepage.dev/group"                            = var.domain
        "traefik.ingress.kubernetes.io/router.middlewares" = "default-authentik-forwardauth@kubernetescrd"
      }
      hosts = [{
        host = "tools.${var.domain}"
        paths = [
          {
            path     = "/"
            pathType = "ImplementationSpecific"
          }
        ]
      }]
    }
  })]
}
