variable "domain" {
  description = "the base domain, from which all subdomains will be derived"
  type        = string
}

variable "smtp" {
  type = object({
    host = string
    port = number
    tls  = bool
  })
  description = "Configuration of the SMTP server"
}

module "postgres" {
  source        = "../postgres"
  name          = "authentik"
  size          = "256Mi"
  vault_secret  = "services/authentik/db"
  chart_version = "15.5.36"
}

resource "random_password" "secret_key" {
  length = 128
}

resource "vault_kv_secret_v2" "secret_key" {
  mount = "kv"
  name  = "services/authentik/secret_key"
  data_json = jsonencode({
    secret-key = random_password.secret_key.result
  })
}

data "vault_generic_secret" "smtp_credentials" {
  path = "kv/services/smtp"
}

resource "kubernetes_secret" "authentik" {
  metadata {
    name = "authentik-credentials"
  }

  data = {
    secret-key    = random_password.secret_key.result
    smtp-username = data.vault_generic_secret.smtp_credentials.data["username"]
    smtp-password = data.vault_generic_secret.smtp_credentials.data["password"]
  }
}

resource "helm_release" "authentik" {
  name        = "authentik"
  version     = "2024.12.1"
  chart       = "authentik"
  repository  = "https://charts.goauthentik.io"
  max_history = 2

  values = [
    yamlencode({
      authentik = {
        log_level = "warning"
        postgresql = {
          host = module.postgres.kubernetes_service_name
        }
        email = {
          host    = var.smtp.host
          port    = var.smtp.port
          use_tls = var.smtp.tls
          from    = "Oecis Authentication <auth@${var.domain}>"
        }
      }
      global = {
        env = [
          {
            name = "AUTHENTIK_POSTGRESQL__PASSWORD"
            valueFrom = {
              secretKeyRef = {
                name = module.postgres.kubernetes_secret_name
                key  = "password"
              }
            }
          },
          {
            name = "AUTHENTIK_SECRET_KEY"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.authentik.metadata[0].name
                key  = "secret-key"
              }
            }
          },
          {
            name = "AUTHENTIK_EMAIL__USERNAME"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.authentik.metadata[0].name
                key  = "smtp-username"
              }
            }
          },
          {
            name = "AUTHENTIK_EMAIL__PASSWORD"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.authentik.metadata[0].name
                key  = "smtp-password"
              }
            }
          }
        ]
      }

      server = {
        ingress = {
          ingressClassName = "traefik"
          enabled          = true
          hosts            = ["authentik.${var.domain}"]
        }
      }

      postgresql = {
        enabled = false
      }

      redis = {
        enabled      = true
        architecture = "standalone"
        master = {
          persistence = {
            enabled = false
          }
        }
      }

    })
  ]
}
