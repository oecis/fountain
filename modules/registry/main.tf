variable "domain" {
  type        = string
  description = "the base domain, from which registry will derive all subdomains"
}

data "vault_generic_secret" "registry_credentials" {
  path = "kv/services/registry"
}

resource "kubernetes_secret" "basic_auth_users" {
  metadata {
    name = "registry-basic-auth-middleware"
  }
  type = "kubernetes.io/basic-auth"

  data = {
    # although the traefik documentation states those values should be base64 encoded,
    # they infact should not be base64 encoded
    username = data.vault_generic_secret.registry_credentials.data["user"]
    password = data.vault_generic_secret.registry_credentials.data["password"]
  }
}

resource "kubernetes_manifest" "basic_auth_middleware" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "basic-auth-registry"
      namespace = "default"
    }
    spec = {
      basicAuth = {
        secret = kubernetes_secret.basic_auth_users.metadata[0].name
      }
    }
  }
}


resource "kubernetes_secret" "oecis_registry_credentials" {
  metadata {
    name = "registry.oecis.io-credentials"
  }

  data = {
    ".dockerconfigjson" = <<DOCKERCONFIG
{"auths":
  {"registry.oecis.io":
    {"auth":
      "${base64encode("${data.vault_generic_secret.registry_credentials.data["user"]}:${data.vault_generic_secret.registry_credentials.data["password"]}")}"
    }
  }
}
DOCKERCONFIG
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_persistent_volume_claim" "this" {
  metadata {
    name = "docker-registry"
    annotations = {
      # The registry will not be included in the backup, as they can be recreated from source easily and are quite big
      "k8up.io/backup" = "false"
    }
  }
  wait_until_bound = false

  spec {
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = "nfs-client"
    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
}

resource "helm_release" "this" {
  name        = "docker-registry"
  repository  = "https://helm.twun.io"
  chart       = "docker-registry"
  version     = "2.2.3"
  max_history = 2

  values = [
    yamlencode({
      persistence = {
        enabled       = true
        existingClaim = kubernetes_persistent_volume_claim.this.metadata[0].name
      }
      ingress = {
        enabled   = true
        className = "traefik"
        hosts     = ["registry.${var.domain}"]
        annotations = {
          "traefik.ingress.kubernetes.io/router.middlewares" = "default-basic-auth-registry@kubernetescrd"
        }
      }
    })
  ]

  depends_on = [kubernetes_manifest.basic_auth_middleware]
}
