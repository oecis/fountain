resource "kubernetes_deployment" "error-pages-deployment" {
  metadata {
    name      = "error-pages"
    namespace = "default"
    labels = {
      app = "error-pages"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "error-pages"
      }
    }

    template {
      metadata {
        labels = {
          app = "error-pages"
        }
      }


      spec {
        container {
          image = "tarampampam/error-pages:3.3.0"
          name  = "error-pages"
          port {
            name           = "http"
            container_port = 8080
          }
          env {
            name  = "TEMPLATE_NAME"
            value = "cats"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "error-pages-service" {
  metadata {
    name      = "error-pages"
    namespace = "default"
  }

  spec {
    selector = {
      "app" = "error-pages"
    }
    port {
      name        = "http"
      port        = 80
      target_port = "http"
    }
  }
}

resource "kubectl_manifest" "error-pages-middleware" {
  yaml_body = file("${path.module}/manifests/error-pages-middleware.yaml")
}

resource "kubectl_manifest" "error-pages-catch-all-ingress" {
  yaml_body = file("${path.module}/manifests/error-pages-catch-all-ingress.yaml")
}
