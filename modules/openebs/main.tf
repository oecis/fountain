resource "helm_release" "openebs" {
  name       = "openebs"
  repository = "https://openebs.github.io/charts"

  chart = "openebs"

  namespace        = "openebs"
  create_namespace = true
}
