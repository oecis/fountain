variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

data "authentik_certificate_key_pair" "default" {
  name = "authentik Self-signed Certificate"
}

resource "authentik_property_mapping_provider_scope" "nextcloud_profile" {
  # Taken from https://docs.goauthentik.io/integrations/services/nextcloud/
  name       = "Nextcloud Profile"
  scope_name = "profile"
  expression = <<EOF
# Extract all groups the user is a member of
groups = [group.name for group in user.ak_groups.all()]

# Nextcloud admins must be members of a group called "admin".
# This is static and cannot be changed.
# We append a fictional "admin" group to the user's groups if they are an admin in authentik.
# This group would only be visible in Nextcloud and does not exist in authentik.
if user.is_superuser and "admin" not in groups:
    groups.append("admin")

return {
    "name": request.user.name,
    "groups": groups,
    # To set a quota set the "nextcloud_quota" property in the user's attributes
    "quota": user.group_attributes().get("nextcloud_quota", None),
    # To connect an already existing user, set the "nextcloud_user_id" property in the
    # user's attributes to the username of the corresponding user on Nextcloud.
    "user_id": user.attributes.get("nextcloud_user_id", str(user.uuid)),
}
EOF
}

data "authentik_property_mapping_provider_scope" "email" {
  managed = "goauthentik.io/providers/oauth2/scope-email"
}

data "authentik_flow" "default-invalidation-flow" {
  slug = "default-provider-invalidation-flow"
}

data "authentik_property_mapping_provider_scope" "openid" {
  managed = "goauthentik.io/providers/oauth2/scope-openid"
}

resource "authentik_provider_oauth2" "this" {
  name               = "nextcloud"
  client_id          = "nextcloud"
  authorization_flow = data.authentik_flow.default-authorization-flow.id
  invalidation_flow  = data.authentik_flow.default-invalidation-flow.id
  signing_key        = data.authentik_certificate_key_pair.default.id
  allowed_redirect_uris = [{
    matching_mode = "strict"
    url           = "https://nextcloud.${var.domain}/apps/user_oidc/code"
  }]
  property_mappings = [
    data.authentik_property_mapping_provider_scope.email.id,
    data.authentik_property_mapping_provider_scope.openid.id,
    authentik_property_mapping_provider_scope.nextcloud_profile.id,
  ]
  sub_mode = "user_uuid"
}

resource "authentik_application" "this" {
  name              = "nextcloud"
  slug              = "nextcloud"
  protocol_provider = authentik_provider_oauth2.this.id
}

module "postgres" {
  source        = "../postgres"
  name          = "nextcloud"
  size          = "2Gi"
  vault_secret  = "services/nextcloud/postgres"
  chart_version = "16.3.0"
}

locals {
  proxy_config_php = <<PROXY-CONFIG
<?php
$CONFIG = array(
  'trusted_proxies' => array(
    0 => '127.0.0.1',
    1 => '10.0.0.0/8',
  ),
  'overwriteprotocol' => 'https',
  'forwarded_for_headers' => array('HTTP_X_FORWARDED_FOR'),
);
PROXY-CONFIG
}

resource "helm_release" "nextcloud" {
  name        = "nextcloud"
  repository  = "https://nextcloud.github.io/helm/"
  chart       = "nextcloud"
  version     = "6.3.2"
  max_history = 2

  values = [
    yamlencode({
      ingress = {
        enabled   = true
        hosts     = "nextcloud.${var.domain}"
        className = "traefik"
        annotations = {
          "gethomepage.dev/enabled"     = "true"
          "gethomepage.dev/name"        = "Nextcloud"
          "gethomepage.dev/description" = "A safe home for all your data – community-driven, free & open source"
          "gethomepage.dev/group"       = var.domain
        }
      }
      cronjob = {
        enabled = true
      }
      # Make nextcloud detect that we are using HTTPS
      # Somehow this does not seem to work and we have to also set it manually using the config files
      phpClientHttpsFix = {
        enabled  = true
        protocol = "https"
      }
      nextcloud = {
        host = "nextcloud.${var.domain}"
        existingSecret = {
          enabled     = true
          secretName  = kubernetes_secret.admin.metadata[0].name
          usernameKey = "username"
          passwordKey = "password"
        }
        configs = {
          # Proxy configuration is required for WebDav to work correctly
          # Taken from the helm chart documentation
          "proxy.config.php" = local.proxy_config_php
        }
      }
      internalDatabase = {
        enabled = false
      }
      externalDatabase = {
        enabled  = true
        type     = "postgresql"
        host     = module.postgres.kubernetes_service_name
        database = "nextcloud"
        user     = "nextcloud"
        existingSecret = {
          enabled     = true
          secretName  = module.postgres.kubernetes_secret_name
          passwordKey = "password"
          usernameKey = "username"
        }
      }
      redis = {
        enabled      = true
        architecture = "standalone"
        auth = {
          enabled = false
        }
      }
      persistence = {
        enabled       = true
        existingClaim = kubernetes_persistent_volume_claim.nextcloud_config.metadata[0].name
        nextcloudData = {
          enabled       = true
          existingClaim = kubernetes_persistent_volume_claim.nextcloud_data.metadata[0].name
        }
      }
      }
    )
  ]
  depends_on = [
    kubernetes_secret.admin,
    kubernetes_secret.db,
    kubernetes_persistent_volume_claim.nextcloud_data,
  ]
}

resource "random_password" "db_postgres" {
  length = 50
}

resource "random_password" "db_replication" {
  length = 50
}

resource "random_password" "db" {
  length = 50
}

resource "kubernetes_secret" "db" {
  metadata {
    name = "nextcloud-db"
  }

  data = {
    username             = "nextcloud"
    password             = random_password.db.result
    postgres-password    = random_password.db_postgres.result
    replication-password = random_password.db_replication.result
  }
}

resource "vault_kv_secret_v2" "db_credentials" {
  mount = "kv"
  name  = "services/nextcloud/db"
  data_json = jsonencode({
    "postgres-password"    = random_password.db_postgres.result
    "replication-password" = random_password.db_replication.result
    password               = random_password.db.result
  })
}

resource "random_password" "admin" {
  length = 50
}

resource "kubernetes_secret" "admin" {
  metadata {
    name = "nextcloud-admin"
  }

  data = {
    username = "admin"
    password = random_password.admin.result
  }
}

resource "vault_kv_secret_v2" "admin_credentials" {
  mount = "kv"
  name  = "services/nextcloud/admin"
  data_json = jsonencode({
    password = random_password.admin.result
  })
}

resource "kubernetes_persistent_volume_claim" "nextcloud_config" {
  metadata {
    name = "nextcloud-config"
  }
  wait_until_bound = false

  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    storage_class_name = "local-path"
  }
}

resource "kubernetes_persistent_volume_claim" "nextcloud_data" {
  metadata {
    name = "nextcloud-data"
    annotations = {
      "k8up.io/backup" = "true"
    }
  }
  wait_until_bound = false

  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "100Gi"
      }
    }
    storage_class_name = "nfs-client"
  }
}

resource "kubernetes_persistent_volume_claim" "nextcloud_db_data" {
  metadata {
    name = "nextcloud-db"
  }
  wait_until_bound = false

  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    storage_class_name = "local-path"
  }
}
