# Deployment

## OIDC

When deploying for the first time, make sure to install the OIDC app in nextcloud

sudo -u www-data ./occ app:enable oidc_login
