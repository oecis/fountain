resource "helm_release" "element-web" {
  # https://artifacthub.io/packages/helm/halkeye/element-web
  repository = "https://halkeye.github.io/helm-charts/"
  chart      = "element-web"
  name       = "element-web"
  values = [
    file("${path.module}/1-element-values.yaml")
  ]
}

resource "kubernetes_manifest" "traefik-header-middleware" {
  # traefik middleware documentation: https://doc.traefik.io/traefik/middlewares/http/headers/
  # element-web documentation: https://github.com/vector-im/element-web#configuration-best-practices
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "element-web-xss-headers"
      namespace = "default"
    }
    spec = {
      headers = {
        customFrameOptionsValue = "SAMEORIGIN"
        browserXssFilter        = true
        contentTypeNosniff      = true
        contentSecurityPolicy   = "frame-ancestors 'none'"
      }
    }
  }

}
