resource "random_password" "db_postgres" {
  length  = 40
  special = false
  lifecycle {
    ignore_changes = [
      special,
      length
    ]
  }
}

resource "random_password" "db_replication" {
  length  = 40
  special = false
  lifecycle {
    ignore_changes = [
      special,
      length
    ]
  }
}

resource "random_password" "db" {
  length  = 40
  special = false
  lifecycle {
    ignore_changes = [
      special,
      length
    ]
  }
}

resource "vault_kv_secret_v2" "db_credentials" {
  mount = "kv"
  name  = var.vault_secret
  data_json = jsonencode({
    "postgres-password"    = random_password.db_postgres.result
    "replication-password" = random_password.db_replication.result
    password               = random_password.db.result
  })
}

resource "kubernetes_secret" "this" {
  metadata {
    name      = "${var.name}-db-credentials"
    namespace = var.namespace
  }

  data = {
    postgres-password    = vault_kv_secret_v2.db_credentials.data["postgres-password"]
    replication-password = vault_kv_secret_v2.db_credentials.data["replication-password"]
    password             = vault_kv_secret_v2.db_credentials.data["password"]
    username             = var.name
  }
}

resource "kubernetes_persistent_volume_claim" "this" {
  metadata {
    name = "${var.name}-db-data"
    annotations = {
      "k8up.io/backup" = "false"
    }
    namespace = var.namespace
  }

  wait_until_bound = false

  spec {
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = "local-path"
    resources {
      requests = {
        storage = var.size
      }
    }
  }
}

resource "helm_release" "this" {
  name = "${var.name}-db"

  repository  = "oci://registry-1.docker.io/bitnamicharts"
  chart       = "postgresql"
  version     = var.chart_version
  max_history = 2
  namespace   = var.namespace

  values = [
    yamlencode({
      architecture = "standalone"
      auth = {
        existingSecret = kubernetes_secret.this.metadata[0].name
        username       = var.name
        database       = var.name
      }
      primary = {
        initdb = {
          args = var.initdb_args
        }
        podAnnotations = {
          "k8up.io/backupcommand"  = "sh -c 'PGDATABASE=\"$POSTGRES_DB\" PGUSER=\"$POSTGRES_USER\" PGPASSWORD=\"$POSTGRES_PASSWORD\" pg_dump --clean'"
          "k8up.io/file-extension" = ".${var.name}.sql"
          "k8up.io/backup"         = "true"
        }
        persistence = {
          enabled       = true
          existingClaim = kubernetes_persistent_volume_claim.this.metadata[0].name
        }
      }
    })
  ]
}
