variable "name" {
  description = "The name of the application, that will be used for the username, database and pod name"
  type        = string
}

variable "size" {
  type        = string
  description = "The size of the PVC"
}

variable "chart_version" {
  type        = string
  description = "The version of the postgres helm chart"
  default     = "14.2.3"
}

variable "vault_secret" {
  type        = string
  description = "The path where to save the generated secret (excluding the mount)"
}

variable "namespace" {
  type        = string
  description = "The kubernetes namespace for this postgres instance"
  default     = "default"
}

variable "initdb_args" {
  type        = string
  description = ""
  default     = ""
}
