output "kubernetes_secret_name" {
  value = kubernetes_secret.this.metadata[0].name
}

output "kubernetes_service_name" {
  value = "${helm_release.this.name}-postgresql"
}

output "db_password" {
  value = random_password.db.result
}
