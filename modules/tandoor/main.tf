variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

module "postgres" {
  source       = "../postgres"
  name         = "tandoor"
  size         = "5Gi"
  vault_secret = "services/tandoor/db"
}

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

data "authentik_flow" "default-invalidation-flow" {
  slug = "default-provider-invalidation-flow"
}

data "authentik_certificate_key_pair" "default" {
  name = "authentik Self-signed Certificate"
}

data "authentik_property_mapping_provider_scope" "this" {
  managed_list = [
    "goauthentik.io/providers/oauth2/scope-email",
    "goauthentik.io/providers/oauth2/scope-openid",
    "goauthentik.io/providers/oauth2/scope-profile"
  ]
}

resource "authentik_provider_oauth2" "this" {
  name               = "tandoor"
  client_id          = "tandoor"
  authorization_flow = data.authentik_flow.default-authorization-flow.id
  invalidation_flow  = data.authentik_flow.default-invalidation-flow.id
  signing_key        = data.authentik_certificate_key_pair.default.id
  allowed_redirect_uris = [
    {
      matching_mode = "strict"
      url           = "https://tandoor.${var.domain}/accounts/oidc/authentik/login/callback/"
    }
  ]
  property_mappings = data.authentik_property_mapping_provider_scope.this.ids
}

resource "authentik_application" "this" {
  name              = "tandoor"
  slug              = "tandoor"
  protocol_provider = authentik_provider_oauth2.this.id
}


locals {
  oidc_config = {
    openid_connect = {
      SERVERS = [
        {
          id                = "authentik"
          name              = "oecis.io"
          server_url        = "https://authentik.${var.domain}/application/o/tandoor"
          token_auth_method = "client_secret_post"
          APP = {
            client_id = authentik_provider_oauth2.this.client_id
            secret    = authentik_provider_oauth2.this.client_secret
          }
        }
      ]
    }
  }
}

resource "helm_release" "tandoor" {
  name        = "tandoor"
  chart       = "tandoor"
  repository  = "https://charts.oecis.io"
  max_history = 2
  version     = "0.1.8"

  values = [
    yamlencode({
      persistence = {
        mediafiles = {
          storageClass = "nfs-client"
          annotations = {
            "k8up.io/backup" = "true"
          }
        }
      }
      tandoor = {
        extraEnv = [
          {
            name  = "SOCIAL_PROVIDERS"
            value = "allauth.socialaccount.providers.openid_connect"
          },
          {
            name  = "ENABLE_SIGNUP"
            value = "0"
          },
          {
            name  = "SOCIALACCOUNT_PROVIDERS"
            value = jsonencode(local.oidc_config)
          },
          {
            name  = "GUNICORN_LOG_LEVEL"
            value = "warning"
          }
        ]
        config = {
          postgres = {
            host = module.postgres.kubernetes_service_name
            password = {
              secretName = module.postgres.kubernetes_secret_name
              secretKey  = "password"
            }
          }
          encryptionKey = {
            secretKey  = "encryption-key"
            secretName = kubernetes_secret.tandoor.metadata[0].name
          }
        }
      }

      ingress = {
        enabled = true
        annotations = {
          "gethomepage.dev/enabled"     = "true"
          "gethomepage.dev/name"        = "Tandoor"
          "gethomepage.dev/description" = "Application for managing recipes, planning meals, building shopping lists and much much more!"
          "gethomepage.dev/group"       = var.domain
        }
        hosts = [
          {
            host = "tandoor.${var.domain}"
          }
        ]
      }
    })
  ]
}

resource "random_password" "encryption_key" {
  length  = 128
  special = false
}

resource "vault_kv_secret_v2" "encryption_key" {
  mount = "kv"
  name  = "services/tandoor/encryption"
  data_json = jsonencode({
    encryption-key = random_password.encryption_key.result
  })
}

resource "kubernetes_secret" "tandoor" {
  metadata {
    name = "tandoor"
  }

  data = {
    encryption-key = random_password.encryption_key.result
  }
}
