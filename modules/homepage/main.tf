variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

resource "authentik_provider_proxy" "this" {
  name                  = "homepage"
  mode                  = "forward_single"
  external_host         = "https://homepage.${var.domain}"
  authorization_flow    = data.authentik_flow.default-authorization-flow.id
  access_token_validity = "days=30"
}

resource "authentik_application" "this" {
  name              = "homepage"
  slug              = "homepage"
  protocol_provider = authentik_provider_proxy.this.id
}

resource "helm_release" "homepage" {
  name        = "homepage"
  chart       = "homepage"
  repository  = "https://jameswynn.github.io/helm-charts"
  version     = "1.2.3"
  max_history = 2

  values = [yamlencode({
    image = {
      repository = "ghcr.io/gethomepage/homepage"
      tag        = "v0.8.8"
    }
    enableRbac = true
    serviceAccount = {
      create = true
    }
    config = {
      bookmarks = []
      services  = []
      widgets = [
        {
          search = {
            "provider" = "custom"
            url        = "https://search.${var.domain}/search?q="
            target     = "_blank"
          }
        }
      ]
      kubernetes = {
        mode = "cluster"
      }
    }
    ingress = {
      main = {
        enabled = true
        annotations = {
          "traefik.ingress.kubernetes.io/router.middlewares" = "default-authentik-forwardauth@kubernetescrd"
        }
        hosts = [{
          host = "homepage.${var.domain}"
          paths = [{
            path     = "/"
            pathType = "Prefix"
          }]
        }]
      }
    }
  })]
}
