variable "host" {
  description = "host of the NFS server"
  type        = string
}

variable "path" {
  description = "path on the NFS server"
  type        = string
}
