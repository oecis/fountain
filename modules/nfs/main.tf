resource "helm_release" "nfs" {
  chart      = "nfs-subdir-external-provisioner"
  name       = "nfs-subdir-external-provisioner"
  repository = "https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner"
  namespace  = "infra"
  set {
    name  = "nfs.server"
    value = var.host
  }
  set {
    name  = "nfs.path"
    value = var.path
  }
}
