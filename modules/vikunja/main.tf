variable "domain" {
  description = "the base domain, from which all subdomains will be derived"
  type        = string
}


variable "smtp" {
  type = object({
    host = string
    port = number
    tls  = bool
  })
  description = "Configuration of the SMTP server"
}

module "postgres" {
  source        = "../postgres"
  name          = "vikunja"
  size          = "256Mi"
  vault_secret  = "services/vikunja/db"
  chart_version = "15.5.36"
}


### JWT ###

resource "random_password" "jwt" {
  length = 64
}

resource "vault_kv_secret_v2" "jwt" {
  mount = "kv"
  name  = "services/vikunja/jwt"
  data_json = jsonencode({
    jwt-secret = random_password.jwt.result
  })
}

resource "kubernetes_secret" "jwt" {
  metadata {
    name = "vikunja-jwt"
  }

  data = {
    jwt-secret = random_password.jwt.result
  }
}

### OIDC ###

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

data "authentik_flow" "default-invalidation-flow" {
  slug = "default-provider-invalidation-flow"
}

data "authentik_certificate_key_pair" "default" {
  name = "authentik Self-signed Certificate"
}

data "authentik_property_mapping_provider_scope" "this" {
  managed_list = [
    "goauthentik.io/providers/oauth2/scope-email",
    "goauthentik.io/providers/oauth2/scope-openid",
    "goauthentik.io/providers/oauth2/scope-profile"
  ]
}

resource "authentik_provider_oauth2" "this" {
  name               = "vikunja"
  client_id          = "vikunja"
  authorization_flow = data.authentik_flow.default-authorization-flow.id
  invalidation_flow  = data.authentik_flow.default-invalidation-flow.id
  signing_key        = data.authentik_certificate_key_pair.default.id
  allowed_redirect_uris = [
    {
      matching_mode = "strict"
      url           = "https://vikunja.${var.domain}/auth/openid/oecisio"
    }
  ]
  property_mappings = data.authentik_property_mapping_provider_scope.this.ids
}

resource "authentik_application" "this" {
  name              = "vikunja"
  slug              = "vikunja"
  protocol_provider = authentik_provider_oauth2.this.id
}

### Vikunja ###

resource "kubernetes_config_map" "oidc" {
  metadata {
    name = "vikunja-oidc"
  }

  data = {
    "oidc-config.yaml" = yamlencode(
      {
        auth = {
          local = {
            enabled = false
          }
          openid = {
            enabled     = true
            redirecturl = "https://vikunja.${var.domain}/auth/openid/"
            providers = [{
              name = var.domain
              # The trailing slash is important so that the authurl and issuer match
              authurl      = "https://authentik.${var.domain}/application/o/vikunja/"
              logouturl    = "https://authentik.${var.domain}/application/o/vikunja/end-session/"
              clientid     = authentik_provider_oauth2.this.client_id
              clientsecret = authentik_provider_oauth2.this.client_secret
              scope        = ["openid", "email", "profile"]
            }]
          }
        }
      }
    )
  }
}

data "vault_generic_secret" "smtp_credentials" {
  path = "kv/services/smtp"
}

resource "helm_release" "vikunja" {
  name        = "vikunja"
  version     = "0.2.2"
  chart       = "vikunja"
  repository  = "https://charts.oecis.io"
  max_history = 2

  dependency_update = true
  values = [
    yamlencode({
      extraVolumes = [
        {
          name = "files"
          emptyDir = {
            medium = "Memory"
          }
        },
        {
          name = "oidc-config"
          configMap = {
            name = kubernetes_config_map.oidc.metadata[0].name
          }
      }]
      extraVolumeMounts = [
        {
          mountPath = "/app/vikunja/files"
          name      = "files"
        },
        {
          name      = "oidc-config"
          mountPath = "/etc/vikunja/config.yaml"
          subPath   = "oidc-config.yaml"
        }
      ]
      ingress = {
        enabled = true
        hosts = [{
          host = "vikunja.${var.domain}"
          paths = [
            {
              path     = "/"
              pathType = "ImplementationSpecific"
            }
          ]
        }]
        annotations = {
          "gethomepage.dev/enabled"     = "true"
          "gethomepage.dev/name"        = "Vikunja"
          "gethomepage.dev/description" = "The to-do app to organize your life."
          "gethomepage.dev/group"       = var.domain
        }
      }
      config = {
        jwt = {
          existingSecret = kubernetes_secret.jwt.metadata[0].name
        }
        cache = {
          enabled = true
          type    = "redis"
          redis = {
            enabled = true
            host    = "vikunja-redis-headless:6379"
          }
        }
        mailer = {
          enabled   = true
          fromemail = "vikunja@${var.domain}"
          authtype  = "login"
          host      = var.smtp.host
          port      = var.smtp.port
          password  = data.vault_generic_secret.smtp_credentials.data["password"]
          username  = data.vault_generic_secret.smtp_credentials.data["username"]
          force_ssl = true
        }
        db = {
          host           = module.postgres.kubernetes_service_name
          user           = "vikunja"
          database       = "vikunja"
          existingSecret = module.postgres.kubernetes_secret_name
          secretKeys = {
            dbPasswordKey = "password"
          }
        }
        publicUrl = "https://vikunja.${var.domain}"
      }
      postgresql = {
        enabled = false
      }
      redis = {
        enabled      = true
        architecture = "standalone"
        master = {
          persistence = {
            enabled = false
          }
        }
      }
    })
  ]
  depends_on = [
    kubernetes_secret.jwt,
    module.postgres
  ]
}
