variable "domain" {
  description = "domain used for the certificate and the traefik dashboard"
  type        = string
}
