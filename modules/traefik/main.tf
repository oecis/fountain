resource "kubernetes_manifest" "authentik_forward_auth" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "authentik-forwardauth"
      namespace = "default"
    }
    spec = {
      forwardAuth = {
        address            = "http://ak-outpost-authentik-embedded-outpost:9000/outpost.goauthentik.io/auth/traefik"
        trustForwardHeader = true
      }
    }
  }
}
data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

resource "authentik_provider_proxy" "this" {
  name               = "traefik"
  mode               = "forward_single"
  external_host      = "https://traefik.${var.domain}"
  authorization_flow = data.authentik_flow.default-authorization-flow.id
}

resource "authentik_application" "this" {
  name              = "traefik"
  slug              = "traefik"
  protocol_provider = authentik_provider_proxy.this.id
}

resource "helm_release" "traefik" {
  name        = "traefik"
  repository  = "https://helm.traefik.io/traefik"
  chart       = "traefik"
  version     = "v32.0.0"
  namespace   = "default"
  max_history = 2

  values = [
    yamlencode({
      ports = {
        # HTTP
        web = {
          redirectTo = {
            port = "websecure"
          }
        }
        # HTTPS
        websecure = {
          asDefault = true
          middlewares = [
            "default-error-pages@kubernetescrd",
          ]
          tls = {
            # the certificate is stored in the default certificate store which uses the wildcard certificate issued by cert-manager
            enabled = true
          }
        }
        dnsovertls = {
          port        = 853
          protocol    = "TCP"
          expose      = { default = true }
          exposedPort = 853
          tls = {
            enabled = true
          }
        }
        ts3udp-voice = {
          port        = 9987
          protocol    = "UDP"
          expose      = { default = true }
          exposedPort = 9987
        }
        ts3tcp-file = {
          port        = 30033
          protocol    = "TCP"
          expose      = { default = true }
          exposedPort = 30033
        }
        ts3tcp-query = {
          port        = 10011
          protocol    = "TCP"
          expose      = { default = true }
          exposedPort = 10011
        }
      }

      ingressClass = {
        enabled        = true
        isDefaultClass = true
      }

      rbac = {
        enabled = true
      }

      logs = {
        access = {
          enabled = false
          format  = "json"
        }
        general = {
          level = "WARN"
        }
      }
      providers = {
        kubernetesIngress = {
          publishedService = {
            enabled = true
          }
        }
        kubernetesCRD = {
          allowCrossNamespace = true
        }
      }

      priorityClassName = "system-cluster-critical"

      service = {
        single         = false
        ipFamilyPolicy = "RequireDualStack"
        type           = "LoadBalancer"
        ipFamilies     = ["IPv4", "IPv6"]
        spec = {
          externalTrafficPolicy         = "Cluster"
          allocateLoadBalancerNodePorts = true
          internalTrafficPolicy         = "Cluster"
        }
        annotations = {
          "metallb.universe.tf/allow-shared-ip" = "treafik-share-key"
        }
      }

      ingressRoute = {
        dashboard = {
          middlewares = [{ name = "authentik-forwardauth" }]
          enabled     = true
          entryPoints = ["websecure"]
          matchRule   = "Host(`traefik.${var.domain}`)"
        }
      }
  })]
  depends_on = [kubernetes_manifest.wildcard-certificate]
}

resource "kubernetes_manifest" "wildcard-certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "${var.domain}-wildcard"
      namespace = "default"
    }
    spec = {
      secretName = "${var.domain}-wildcard-certificate"
      dnsNames = [
        var.domain,
        "*.${var.domain}"
      ]
      issuerRef = {
        name = "wildcard"
        kind = "ClusterIssuer"
      }
    }
  }
}

resource "kubernetes_manifest" "tls-store" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "TLSStore"
    metadata = {
      name      = "default"
      namespace = "default"
    }
    spec = {
      defaultCertificate = {
        secretName = "${var.domain}-wildcard-certificate"
      }
    }
  }
  depends_on = [
    helm_release.traefik
  ]
}
