variable "domain" {
  type        = string
  description = "the base domain, from which kratos will derive all subdomains"
}

variable "smtp" {
  type = object({
    host = string
    port = number
    tls  = bool
  })
  description = "Configuration of the SMTP server"
}

module "postgres" {
  source       = "../postgres"
  name         = "kratos"
  size         = "256Mi"
  vault_secret = "services/ory/kratos/db"
}

resource "random_password" "encryption_key_cookie" {
  length = 129
}

resource "random_password" "encryption_key_default" {
  length = 129
}

data "vault_generic_secret" "smtp_credentials" {
  path = "kv/services/smtp"
}

resource "kubernetes_secret" "kratos" {
  metadata {
    name = "kratos"
  }

  data = {
    dsn               = "postgres://kratos:${module.postgres.db_password}@kratos-db-postgresql/kratos"
    secretsCookie     = random_password.encryption_key_cookie.result
    secretsDefault    = random_password.encryption_key_default.result
    smtpConnectionURI = "smtp${var.smtp.tls ? "s" : ""}://${data.vault_generic_secret.smtp_credentials.data["username"]}:${urlencode(data.vault_generic_secret.smtp_credentials.data["password"])}@${var.smtp.host}:${var.smtp.port}${var.smtp.tls ? "" : "?disable_starttls=true"}"
  }
}

resource "vault_kv_secret_v2" "encryption_keys" {
  mount = "kv"
  name  = "services/ory/hdyra/encryption"
  data_json = jsonencode({
    cookie  = random_password.encryption_key_cookie.result
    default = random_password.encryption_key_default.result
  })
}


resource "helm_release" "kratos" {
  name        = "kratos"
  version     = "0.39.1"
  max_history = 2

  repository = "https://k8s.ory.sh/helm/charts"
  chart      = "kratos"

  values = [
    yamlencode({
      kratos = {
        automigration = {
          enabled = true
        }
        config = {
          oauth2_provider = {
            url = "http://hydra-admin:4445"
          }
          log = {
            level = "error"
          }
          courier = {
            smtp = {
              from_address = "auth@${var.domain}"
              from_name    = "Oecis Authentication"
            }
          }
          cookies = {
            domain    = var.domain
            same_site = "Lax"
          }
          session = {
            lifespan                 = "720h"
            earliest_possible_extend = "42h"
            cookie = {
              domain    = var.domain
              same_site = "Lax"
            }
          }
          serve = {
            public = {
              base_url = "https://kratos.${var.domain}"
              request_log = {
                # don't log k8s health checks
                disable_for_health = true
              }
            }
            admin = {
              base_url = "http://kratos-admin:4434"
              request_log = {
                # don't log k8s health checks
                disable_for_health = true
              }
            }
          }
          identity = {
            schemas = [
              {
                id  = "default"
                url = "base64://${base64encode(file("${path.module}/schemas/identity.default.schema.json"))}"
              }
            ]
          }
          selfservice = {
            default_browser_return_url = "https://${var.domain}"
            allowed_return_urls = [
              "https://${var.domain}",
              "https://*.${var.domain}"
            ]
            methods = {
              password = {
                enabled = true
              }
              link = {
                enabled = false
              }
              totp = {
                enabled = true
                config = {
                  issuer = var.domain
                }
              }
              code = {
                enabled = true
                config = {
                  lifespan = "2h"
                }
              }
              webauthn = {
                enabled = true
                config = {
                  passwordless = true
                  rp = {
                    display_name = "oecis"
                    id           = var.domain
                    origin       = "https://${var.domain}/auth/register"
                  }
                }
              }
            }
            flows = {
              error = {
                ui_url = "https://${var.domain}/auth/error"
              }
              settings = {
                ui_url                     = "https://${var.domain}/auth/settings"
                privileged_session_max_age = "15m"
              }
              recovery = {
                enabled  = true
                ui_url   = "https://${var.domain}/auth/recovery"
                lifespan = "15m"
                use      = "code"
                after = {
                  hooks = [
                    {
                      hook = "revoke_active_sessions"
                    }
                  ]
                }
              }
              verification = {
                enabled = true
                ui_url  = "https://${var.domain}/auth/verification"
                use     = "code"
                after = {
                  default_browser_return_url = "https://${var.domain}/"
                }
              }
              logout = {
                after = {
                  default_browser_return_url = "https://${var.domain}"
                }
              }
              login = {
                ui_url   = "https://${var.domain}/auth/login"
                lifespan = "10m"
                after = {
                  password = {
                    hooks = [
                      {
                        hook = "require_verified_address"
                      }
                    ]
                  }
                }
              }
              registration = {
                enabled  = false
                lifespan = "10m"
                ui_url   = "https://${var.domain}/auth/register"
                after = {
                  default_browser_return_url = "https://kratos.${var.domain}/self-service/verification/browser"
                }
              }
            }
          }
        }
      }

      deployment = {
        extraEnv = [
          {
            name = "COURIER_SMTP_CONNECTION_URI"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.kratos.metadata[0].name
                key  = "smtpConnectionURI"
              }
            }
          }
        ]
      }

      statefullset = {
        extraEnv = [
          {
            name = "COURIER_SMTP_CONNECTION_URI"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.kratos.metadata[0].name
                key  = "smtpConnectionURI"
              }
            }
          }
        ]
      }

      ingress = {
        public = {
          enabled   = true
          className = "traefik"
          hosts = [
            {
              host = "kratos.${var.domain}"
              paths = [
                {
                  path     = "/"
                  pathType = "ImplementationSpecific"
                }
              ]
            }
          ]
        }
      }

      secret = {
        enabled      = false
        nameOverride = kubernetes_secret.kratos.metadata[0].name
    } })
  ]

  depends_on = [
    module.postgres,
    kubernetes_secret.kratos
  ]
}
