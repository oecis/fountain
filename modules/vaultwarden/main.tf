variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

variable "smtp" {
  type = object({
    host = string
    port = number
    tls  = bool
  })
  description = "Configuration of the SMTP server"
}

module "postgres" {
  source       = "../postgres"
  name         = "vaultwarden"
  size         = "256Mi"
  vault_secret = "services/vaultwarden/db"
}

resource "kubernetes_secret" "vaultwarden_db_uri" {
  metadata {
    name = "vaultwarden-db-uri"
  }

  data = {
    uri = "postgres://vaultwarden:${module.postgres.db_password}@vaultwarden-db-postgresql/vaultwarden"
  }
}

### Admin token ###

resource "random_password" "admin" {
  length = 40
}

resource "kubernetes_secret" "admin_token" {
  metadata {
    name = "vaultwarden-admin-token"
  }

  data = {
    token = random_password.admin.result
  }
}

resource "vault_kv_secret_v2" "admin_token" {
  mount = "kv"
  name  = "services/vaultwarden/admin"
  data_json = jsonencode({
    token = random_password.admin.result
  })
}

### SMTP ###

data "vault_generic_secret" "smtp_credentials" {
  path = "kv/services/smtp"
}

resource "kubernetes_secret" "vaultwarden_smtp_credentials_secret" {
  metadata {
    name = "vaultwarden-smtp-credentials"
  }

  data = {
    username = data.vault_generic_secret.smtp_credentials.data["username"]
    password = data.vault_generic_secret.smtp_credentials.data["password"]
  }
}


resource "helm_release" "vaultwarden" {
  name        = "vaultwarden"
  chart       = "vaultwarden"
  repository  = "https://charts.oecis.io"
  version     = "0.31.4"
  max_history = 2

  values = [
    yamlencode({
      ingress = {
        enabled  = true
        class    = "traefik"
        hostname = "vaultwarden.${var.domain}"
        additionalAnnotations = {
          "gethomepage.dev/enabled"     = "true"
          "gethomepage.dev/name"        = "Vaultwarden"
          "gethomepage.dev/description" = "Password Manager"
          "gethomepage.dev/group"       = var.domain
        }
      }
      timeZone           = "Europe/Berlin"
      domain             = "https://vaultwarden.${var.domain}"
      signupsAllowed     = false
      invitationsAllowed = true
      signupsDomain      = var.domain
      adminToken = {
        existingSecret    = kubernetes_secret.admin_token.metadata[0].name
        existingSecretKey = "token"
      }
      rocket = {
        address = "::"
      }
      websocket = {
        address = "::"
      }
      smtp = {
        host           = var.smtp.host
        port           = var.smtp.port
        security       = var.smtp.tls ? "force_tls" : "off"
        from           = "vaultwarden@${var.domain}"
        fromName       = "oecis.io"
        existingSecret = kubernetes_secret.vaultwarden_smtp_credentials_secret.metadata[0].name
        username = {
          existingSecretKey = "username"
        }
        password = {
          existingSecretKey = "password"
        }
      }
      database = {
        type              = "postgresql"
        existingSecret    = kubernetes_secret.vaultwarden_db_uri.metadata[0].name
        existingSecretKey = "uri"
      }
      storage = {
        enabled = true
        size    = "10Gi"
        class   = "nfs-client"
      }
    })
  ]
  depends_on = [
    module.postgres,
    kubernetes_secret.vaultwarden_db_uri,
    kubernetes_secret.admin_token,
    kubernetes_secret.vaultwarden_smtp_credentials_secret
  ]
}
