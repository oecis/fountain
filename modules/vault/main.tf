resource "helm_release" "vault" {
  name       = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  version    = "v0.28.1"

  namespace = "default"

  values = [
    yamlencode({
      server = {
        annotations = {
          "k8up.io/backupcommand"  = "vault operator raft snapshot save backup.snap"
          "k8up.io/file-extension" = ".snap"
        }
        ha = {
          enabled  = true
          replicas = 1
          raft = {
            enabled = true
          }
        }
        ingress = {
          enabled = true
          hosts = [
            {
              host  = "vault.${var.domain}"
              paths = []
            }
          ]
        }
      }
    })
  ]
}
