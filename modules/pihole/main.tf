variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

resource "random_password" "admin" {
  length = 40
}

resource "kubernetes_secret" "admin" {
  metadata {
    name = "pihole-admin"
  }

  data = {
    password = random_password.admin.result
  }
}

resource "vault_kv_secret_v2" "admin" {
  mount = "kv"
  name  = "services/pihole/admin"
  data_json = jsonencode({
    username = "admin"
    password = random_password.admin.result
  })
}

resource "kubernetes_persistent_volume_claim" "this" {
  metadata {
    name = "pihole"
  }
  wait_until_bound = false
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    storage_class_name = "local-path"
  }
}

resource "helm_release" "pihole" {
  name        = "pihole"
  chart       = "pihole"
  repository  = "https://mojo2600.github.io/pihole-kubernetes/"
  max_history = 2
  version     = "2.27.0"

  values = [yamlencode({
    DNS1 = "10.43.25.169" # static IP address of unbound
    DNS2 = "invalid"      # disable the second one, because otherwise Google will be used
    dualStack = {
      enabled = true
    }
    # adlists cannot be added after installation. pihole has to be completly reinstalled after adding a adlist
    adlists = [
      "https://blocklistproject.github.io/Lists/ads.txt",
      "https://blocklistproject.github.io/Lists/malware.txt",
      "https://blocklistproject.github.io/Lists/scam.txt",
      "https://blocklistproject.github.io/Lists/tracking.txt"
    ]
    persistentVolumeClaim = {
      enabled       = true
      existingClaim = kubernetes_persistent_volume_claim.this.metadata[0].name
    }
    admin = {
      existingSecret = kubernetes_secret.admin.metadata[0].name
      passwordKey    = "password"
    }
    virtualHost = "pihole.${var.domain}"
    ingress = {
      enabled = true
      hosts   = ["pihole.${var.domain}"]
    }
    podDnsConfig = {
      enabled = false
    }
    serviceDns = {
      # We want two different services for tcp and udp
      mixedService = false
      type         = "ClusterIP"
    }
    serviceDhcp = {
      enabled = false
    }
    extraEnvVars = {
      QUERY_LOGGING = "false"
    }
    probes = {
      # The initialDelaySeconds are originally set to 60 seconds, which is a bit too much
      liveness = {
        initialDelaySeconds = 15
        type                = "command"
        command = [
          "/bin/bash",
          "-c",
          "curl http://localhost/admin/index.php && dig +short @localhost example.com",
        ]
      }
      readiness = {
        initialDelaySeconds = 15
      }
    }
  })]
}

resource "kubernetes_manifest" "ingress-route-tls" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "IngressRouteTCP"
    metadata = {
      name      = "pihole-ingress-route-tls"
      namespace = "default"
    }
    spec = {
      # somehow tls is not picked up by default, like with the HTTP ingresses.
      # Probably because here we are using this IngressRouteTCP resources
      # instead of the standard k8s ingress used everywhere else
      tls         = {}
      entryPoints = ["dnsovertls"]
      routes = [{
        match = "HostSNI(`dns.${var.domain}`)"
        services = [{
          name = "pihole-dns-tcp"
          port = 53
        }]
      }]
    }
  }
}
