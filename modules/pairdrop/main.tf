variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

data "authentik_flow" "default-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

resource "authentik_provider_proxy" "this" {
  name                  = "pairdrop"
  mode                  = "forward_single"
  external_host         = "https://pairdrop.${var.domain}"
  authorization_flow    = data.authentik_flow.default-authorization-flow.id
  access_token_validity = "days=30"
}

resource "authentik_application" "this" {
  name              = "pairdrop"
  slug              = "pairdrop"
  protocol_provider = authentik_provider_proxy.this.id
}

resource "helm_release" "this" {
  name        = "pairdrop"
  repository  = "https://charts.oecis.io"
  chart       = "pairdrop"
  version     = "0.1.0"
  max_history = 2

  values = [
    yamlencode({
      ingress = {
        enabled = true
        annotations = {
          "gethomepage.dev/enabled"                          = "true"
          "gethomepage.dev/name"                             = "Pairdrop"
          "gethomepage.dev/description"                      = "Local file sharing in your browser"
          "gethomepage.dev/group"                            = var.domain
          "traefik.ingress.kubernetes.io/router.middlewares" = "default-authentik-forwardauth@kubernetescrd"
        }
        hosts = [
          {
            host = "pairdrop.${var.domain}"
            paths = [
              {
                path     = "/"
                pathType = "ImplementationSpecific"
              }
            ]
          }
        ]
      }
    })
  ]
}
