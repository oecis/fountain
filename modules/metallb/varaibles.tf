variable "addresses" {
  description = "List of public IP addresses of the cluster in cidr form (IPv4 and IPv6)"
  type        = list(string)
}
