resource "kubernetes_namespace" "this" {
  metadata {
    name = "metallb-system"

    labels = {
      "pod-security.kubernetes.io/enforce" = "privileged"
      "pod-security.kubernetes.io/audit"   = "privileged"
      "pod-security.kubernetes.io/warn"    = "privileged"
    }
  }
}

resource "helm_release" "metallb" {
  name        = "metallb"
  repository  = "https://metallb.github.io/metallb"
  chart       = "metallb"
  namespace   = kubernetes_namespace.this.metadata[0].name
  max_history = 2
  version     = "0.13.12"

  values = [
    yamlencode({
      speaker = {
        livenessProbe = {
          enabled = false
        }
        readinessProbe = {
          enabled = false
        }
        frr = {
          enabled = false
        }
      }
    })
  ]
}

resource "kubectl_manifest" "metallb-default-ip-pool" {
  yaml_body = yamlencode({
    apiVersion = "metallb.io/v1beta1"
    kind       = "IPAddressPool"
    metadata = {
      name      = "default-pool"
      namespace = kubernetes_namespace.this.metadata[0].name
    }
    spec = {
      addresses = var.addresses
    }
  })
  depends_on = [
    helm_release.metallb
  ]
}

resource "kubectl_manifest" "metallb-l2-advertisment" {
  yaml_body = yamlencode({
    apiVersion = "metallb.io/v1beta1"
    kind       = "L2Advertisement"
    metadata = {
      name      = "default"
      namespace = kubernetes_namespace.this.metadata[0].name
    }
    spec = {
      ipAddressPools = ["default-pool"]
    }
  })
  depends_on = [
    resource.kubectl_manifest.metallb-default-ip-pool
  ]
}
