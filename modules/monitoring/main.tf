resource "helm_release" "kube_prometheus_stack" {
  name        = "kube-prometheus-stack"
  chart       = "kube-prometheus-stack"
  repository  = "https://prometheus-community.github.io/helm-charts"
  max_history = 2


  values = [yamlencode({
    # deactivate cluster monitoring for now
    defaultRules = {
      create = false
    }
    # for now, we only need prometheus
    alertmanager = {
      enabled = false
    }
    kubeStateMetrics = {
      enabled = false
    }
    nodeExporter = {
      enabled = false
    }
    grafana = {
      enabled = false
    }
  })]
}

resource "helm_release" "prometheus_pushgateway" {
  name        = "prometheus-pushgateway"
  chart       = "prometheus-pushgateway"
  repository  = "https://prometheus-community.github.io/helm-charts"
  max_history = 2
}
