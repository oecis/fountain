variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

resource "helm_release" "unbound" {
  name        = "unbound"
  chart       = "unbound"
  repository  = "https://pixelfederation.github.io/unbound"
  namespace   = "default"
  version     = "0.1.3"
  max_history = 2

  values = [
    yamlencode(
      {
        # Default is 3, but I think for the beginning we don't need that much
        replicaCount = 1
        # We need a static IP address so that pihole can reach unbound
        clusterIP = "10.43.25.169"
        containers = {
          unbound = {
            config = {
              # Disable all logging
              verbosity        = 0
              logReplies       = "no"
              logQueries       = "no"
              logTagQueryreply = "no"
              logLocalActions  = "no"
              logServfail      = "no"
              doTcp            = "yes"
              doIp6            = "yes"
            }
          }
        }
    })
  ]
}
