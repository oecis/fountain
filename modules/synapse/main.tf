variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

resource "kubernetes_namespace" "this" {
  metadata {
    name = "matrix"
  }
}

module "oidc_client" {
  source                     = "../hydra-oidc-client"
  name                       = "synapse"
  redirect_uri               = "https://matrix.${var.domain}/_synapse/client/oidc/callback"
  token_endpoint_auth_method = "client_secret_basic"
  namespace                  = kubernetes_namespace.this.metadata[0].name
}

# Not an ideal solution, but synapse cannot read secrets from env variables
data "kubernetes_secret" "oidc_client" {
  metadata {
    name      = module.oidc_client.secret_name
    namespace = kubernetes_namespace.this.metadata[0].name
  }
  depends_on = [module.oidc_client]
}

module "postgres" {
  source       = "../postgres"
  name         = "synapse"
  size         = "256Mi"
  vault_secret = "services/synapse/db"
  namespace    = kubernetes_namespace.this.metadata[0].name
  # Special configuration required by synapse...
  initdb_args = "--encoding=UTF8 --locale=C"
}

resource "helm_release" "this" {
  name       = "synapse"
  repository = "https://ananace.gitlab.io/charts"
  chart      = "matrix-synapse"
  namespace  = kubernetes_namespace.this.metadata[0].name

  values = [
    yamlencode({
      serverName       = "matrix.${var.domain}"
      publicServerName = "matrix.${var.domain}"
      signingkey       = { job = { enabled = false } }
      wellknown = {
        enabled = true
        useIpv6 = true
      }
      postgresql = {
        # Disable the internal postgres included in the chart
        enabled = false
      }
      externalPostgresql = {
        host                      = module.postgres.kubernetes_service_name
        existingSecret            = module.postgres.kubernetes_secret_name
        existingSecretPasswordKey = "password"
      }
      ingress = {
        enabled           = true
        traefikPaths      = true
        hosts             = ["matrix.${var.domain}"]
        csHosts           = ["matrix.${var.domain}"]
        includeServerName = true
      }
      extraConfig = {
        oidc_providers = [
          {
            idp_id        = "hydra"
            idp_name      = "oecis.io"
            discover      = true
            issuer        = "https://hydra.${var.domain}"
            client_id     = data.kubernetes_secret.oidc_client.data["CLIENT_ID"]
            client_secret = data.kubernetes_secret.oidc_client.data["CLIENT_SECRET"]
            scopes        = ["openid", "profile"]
          }
        ]
      }
    })
  ]
}
