resource "helm_release" "hortusfox" {
  name        = "hortusfox"
  chart       = "hortusfox"
  repository  = "https://charts.oecis.io"
  max_history = 2
  version     = "0.1.0"

  values = [yamlencode({
  })]
}
