variable "domain" {
  type        = string
  description = "the base domain, from which hydra will derive its own subdomain"
}

module "postgres" {
  source       = "../postgres"
  name         = "hydra"
  vault_secret = "services/ory/hydra/db"
  size         = "256Mi"
}

resource "random_password" "oidc_salt" {
  length = 50
}

resource "random_password" "encryption_key_cookie" {
  length = 128
}

resource "random_password" "encryption_key_system" {
  length = 128
}


resource "kubernetes_secret" "hydra" {
  metadata {
    name = "hydra"
  }

  data = {
    dsn           = "postgres://hydra:${module.postgres.db_password}@hydra-db-postgresql/hydra"
    secretsCookie = random_password.encryption_key_cookie.result
    secretsSystem = random_password.encryption_key_system.result
  }
}

resource "vault_kv_secret_v2" "encryption_keys" {
  mount = "kv"
  name  = "services/ory/hdyra/encryption"
  data_json = jsonencode({
    cookie = random_password.encryption_key_cookie.result
    system = random_password.encryption_key_system.result
  })
}


resource "helm_release" "hydra" {
  name        = "hydra"
  version     = "0.45.0"
  max_history = 2

  repository = "https://k8s.ory.sh/helm/charts"
  chart      = "hydra"

  values = [
    yamlencode({
      maester = {
        enabled = true
      }
      deployment = {
        extraEnv = [{
          name = "DSN"
          valueFrom = {
            secretKeyRef = {
              name = kubernetes_secret.hydra.metadata[0].name
              key  = "dsn"
            }
          }
        }]
        automigration = {
          extraEnv = [{
            name = "DSN"
            valueFrom = {
              secretKeyRef = {
                name = kubernetes_secret.hydra.metadata[0].name
                key  = "dsn"
              }
            }
          }]
        }
      }
      secret = {
        enabled      = false
        nameOverride = kubernetes_secret.hydra.metadata[0].name
      }
      ingress = {
        public = {
          enabled   = true
          className = "traefik"
          hosts = [{
            host = "hydra.${var.domain}"
            paths = [{
              path     = "/"
              pathType = "ImplementationSpecific"
            }]
          }]
        }
      }
      hydra = {
        automigration = {
          enabled = true
        }
        config = {
          log = {
            format = "text"
            level  = "error"
          }
          serve = {
            cookies = {
              domain         = var.domain
              same_site_mode = "Lax"
            }
            public = {
              cors = {
                allowed_origins = [
                  "https://hydra.${var.domain}"
                ]
              }
              request_log = {
                # don't log k8s health checks
                disable_for_health = true
              }
            }
            admin = {
              request_log = {
                disable_for_health = true
              }
            }
          }
          ttl = {
            access_token  = "1h"
            refresh_token = "720h"
          }
          urls = {
            login   = "https://kratos.${var.domain}/self-service/login/browser"
            consent = "https://${var.domain}/auth/consent"
            logout  = "https://kratos.${var.domain}/self-service/logout/browser"
            error   = "https://${var.domain}/auth/error"
            self = {
              public = "https://hydra.${var.domain}"
              admin  = "http://hydra-admin:4445"
              issuer = "https://hydra.${var.domain}"
            }
          }
          oidc = {
            subject_identifiers = {
              supported_types = [
                "pairwise",
                "public"
              ]
              pairwise = {
                salt = random_password.oidc_salt.result
              }
            }
          }
        }
      }
    })
  ]

  depends_on = [
    module.postgres,
    kubernetes_secret.hydra
  ]
}
