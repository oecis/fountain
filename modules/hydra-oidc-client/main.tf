locals {
  secret_name = "${var.name}-oauth2-client-secret"
}

resource "kubernetes_manifest" "this" {
  manifest = {
    apiVersion = "hydra.ory.sh/v1alpha1"
    kind       = "OAuth2Client"
    metadata = {
      name      = var.name
      namespace = var.namespace
    }
    spec = {
      grantTypes              = var.grant_types
      responseTypes           = var.response_types
      scope                   = var.scopes
      secretName              = local.secret_name
      tokenEndpointAuthMethod = var.token_endpoint_auth_method
      redirectUris = [
        var.redirect_uri
      ]
    }
  }
}
