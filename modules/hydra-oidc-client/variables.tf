variable "name" {
  description = "name of the OAuth2Client"
  type        = string
}

variable "namespace" {
  description = "Namespace for the client"
  type        = string
  default     = "default"
}

variable "redirect_uri" {
  description = "App specific redirect URI"
  type        = string
}

variable "scopes" {
  description = "scopes for the OAuth2Client"
  default     = "openid profile email"
  type        = string
}

variable "token_endpoint_auth_method" {
  description = "One of 'client_secret_post' or 'client_secret_basic'"
  type        = string
  default     = "client_secret_post"
}

variable "response_types" {
  type    = list(string)
  default = ["id_token", "code", "token"]
}
variable "grant_types" {
  type    = list(string)
  default = ["client_credentials", "authorization_code", "refresh_token"]
}
