resource "kubernetes_manifest" "bootstrap_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "bootstrap"
    }
    spec = {
      selfSigned = {}
    }
  }
}

resource "kubernetes_manifest" "ca_cert" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "ca-cert"
      namespace = "cert-manager"
    }
    spec = {
      isCA       = true
      commonName = "CA"
      secretName = "ca-secret"
      privateKey = {
        algorithm = "ECDSA"
        size      = 256
      }
      issuerRef = {
        name  = kubernetes_manifest.bootstrap_issuer.manifest.metadata.name
        kind  = kubernetes_manifest.bootstrap_issuer.manifest.kind
        group = "cert-manager.io"
      }
    }
  }
}

resource "kubernetes_manifest" "cluster_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "wildcard"
    }
    spec = {
      ca = {
        secretName = kubernetes_manifest.ca_cert.manifest.spec.secretName
      }
    }
  }
}
