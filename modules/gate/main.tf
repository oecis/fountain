variable "gate_routes" {
  type        = list(any)
  description = "The routes configuration for gate"
}

resource "helm_release" "gate" {
  repository  = "https://charts.oecis.io"
  chart       = "gate"
  name        = "gate"
  version     = "0.1.1"
  max_history = 2


  values = [
    yamlencode({
      gate = {
        config = {
          # gate config.yml starts here
          config = {
            lite = {
              enabled = true
              routes  = var.gate_routes
            }
          }
        }
      }
    })
  ]
}

resource "kubernetes_manifest" "ingress_route_tcp" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "IngressRouteTCP"
    metadata = {
      name      = "gate-ingress-route-tcp"
      namespace = "default"
    }
    spec = {
      entryPoints = ["minecraft"]
      routes = [{
        match = "HostSNI(`*`)"
        services = [{
          name = "gate"
          port = 25565
        }]
      }]
    }
  }
}
