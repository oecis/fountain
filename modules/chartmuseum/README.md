# chartmuseum

helm chart storage for custom oecis helm charts

## Auth

Use internal (chartmusuem) basic auth, because there is no easy way to connect it with ory. Maybe we could use JWTs but that seemed like too much overhead for something so simple.
