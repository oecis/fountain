variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}

resource "kubernetes_persistent_volume_claim" "charts_storage" {
  metadata {
    name = "charts-storage"
  }
  wait_until_bound = false

  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    storage_class_name = "local-path"
  }
}

data "vault_generic_secret" "chartmuseum_basic_auth" {
  path = "kv/services/chartmuseum/auth"
}

resource "kubernetes_secret" "chartmuseum_basic_auth_secret" {
  metadata {
    name = "chartmuseum-basic-auth"
  }

  data = {
    username = data.vault_generic_secret.chartmuseum_basic_auth.data["username"]
    password = data.vault_generic_secret.chartmuseum_basic_auth.data["password"]
  }

}

resource "helm_release" "chartmuseum" {
  name       = "chartmuseum"
  repository = "https://chartmuseum.github.io/charts"
  chart      = "chartmuseum"

  namespace = "default"

  values = [
    yamlencode({
      env = {
        existingSecret = kubernetes_secret.chartmuseum_basic_auth_secret.metadata[0].name
        existingSecretMappings = {
          BASIC_AUTH_USER     = "username"
          BASIC_AUTH_PASSWORD = "password"
        }
        open = {
          STORAGE     = "local"
          DISABLE_API = false
          # We allow open access to download the charts and only require auth for write operations
          AUTH_ANONYMOUS_GET = true
          CHART_URL          = "https://charts.${var.domain}"
        }
      }
      persistence = {
        enabled       = true
        existingClaim = kubernetes_persistent_volume_claim.charts_storage.metadata[0].name
      }
      replicaCount = 1
      ingress = {
        enabled          = true
        ingressClassName = "traefik"
        hosts = [{
          name = "charts.${var.domain}"
        }]
      }
    })
  ]

  depends_on = [
    kubernetes_persistent_volume_claim.charts_storage
  ]
}
