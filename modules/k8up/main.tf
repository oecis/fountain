resource "helm_release" "k8up" {
  name       = "k8up"
  chart      = "k8up"
  repository = "https://k8up-io.github.io/k8up"
  version    = "4.7.0"

  values = [yamlencode({
    k8up = {
      skipWithoutAnnotation = true
      envVars = [{
        name  = "BACKUP_PROMURL"
        value = "http://prometheus-pushgateway:9091"
      }]
    }
  })]
}

# Apply all relevant CRDs
data "http" "crds" {
  url = "https://github.com/k8up-io/k8up/releases/download/k8up-4.7.0/k8up-crd.yaml"
}

resource "kubernetes_manifest" "crds" {
  for_each = {
    for manifest in [
    for yaml in split("\n---\n", data.http.crds.response_body) : yamldecode(yaml)] :
    manifest["metadata"]["name"] => manifest
  }
  manifest = each.value
}

resource "random_password" "repo_key" {
  length  = 40
  special = false
}

resource "vault_kv_secret_v2" "repo_key" {
  mount = "kv"
  name  = "services/k8up/repo"
  data_json = jsonencode({
    password = random_password.repo_key.result
  })
}

resource "kubernetes_secret" "repo_key" {
  metadata {
    name      = "backup-repo"
    namespace = "default"
  }

  data = {
    password = random_password.repo_key.result
  }
}

# Add the minio credentials to the cluster
data "vault_kv_secrets_list_v2" "targets" {
  mount = "kv"
  name  = "services/k8up/targets"
}

locals {
  targets = toset([for name in nonsensitive(toset(data.vault_kv_secrets_list_v2.targets.names)) : trimsuffix(name, "/")])
}

data "vault_generic_secret" "targets" {
  for_each = local.targets
  path     = "kv/services/k8up/targets/${each.value}/s3-credentials"
}

resource "kubernetes_secret" "backup_target_s3_credentials" {
  for_each = data.vault_generic_secret.targets
  metadata {
    name      = "${each.key}-s3-credentials"
    namespace = "default"
  }

  data = {
    access-key = each.value.data["access-key"]
    secret-key = each.value.data["secret-key"]
  }
}
