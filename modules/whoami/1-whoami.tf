resource "kubernetes_deployment" "whoami_deployment" {

  metadata {
    name      = "whoami"
    namespace = "default"
    labels = {
      app = "whoami"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "whoami"
      }
    }

    template {
      metadata {
        labels = {
          app = "whoami"
        }
      }

      spec {
        container {
          image = "traefik/whoami"
          name  = "whoami"
          port {
            container_port = 80
            name           = "web"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "whoami_service" {

  metadata {
    name      = "whoami"
    namespace = "default"
  }

  spec {
    selector = {
      "app" = "whoami"
    }

    port {
      name        = "web"
      port        = 80
      target_port = "web"
    }
  }
}

resource "kubectl_manifest" "whoami_ingress" {
  yaml_body = file("${path.module}/manifests/whoami-ingress.yaml")
}
