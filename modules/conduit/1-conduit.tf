resource "helm_release" "conduit" {
  # https://artifacthub.io/packages/helm/cronce/conduit
  repository = "https://charts.cronce.io/"
  chart      = "conduit"
  name       = "conduit"
  values = [
    file("${path.module}/1-conduit-values.yaml")
  ]
}
