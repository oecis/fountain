variable "domain" {
  type        = string
  description = "the base domain, from which mirage will derive all subdomains"
}

resource "random_password" "secret_base" {
  length = 88
}

resource "kubernetes_secret" "mirage" {
  metadata {
    name = "mirage"
  }
  data = {
    secretKeyBase = random_password.secret_base.result
  }
}

resource "vault_kv_secret_v2" "secret_base" {
  mount = "kv"
  name  = "services/mirage/secret"
  data_json = jsonencode({
    secret-base = random_password.secret_base.result
  })
}

resource "helm_release" "mirage" {
  name       = "oecis-mirage"
  chart      = "mirage"
  repository = "https://charts.oecis.io/"
  version    = "0.1.2"

  values = [
    yamlencode({
      imagePullSecrets = [
        {
          name = "registry.oecis.io-credentials"
        }
      ]

      ingress = {
        enabled   = true
        className = "traefik"
        hosts = [
          {
            host = var.domain
            paths = [
              {
                path     = "/"
                pathType = "ImplementationSpecific"
              }
            ]
          }
        ]
      }

      mirage = {
        secretName  = kubernetes_secret.mirage.metadata[0].name
        hostBaseUrl = var.domain
        kratos = {
          external = "https://kratos.${var.domain}"
          internal = "http://kratos-public"
        }
        hydra = {
          internal = "http://hydra-admin:4445"
        }
      }
    })
  ]
}
