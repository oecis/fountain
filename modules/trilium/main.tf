resource "helm_release" "trilium" {
  # Name cannot be "trillium" otherwise there will be funny conflicts
  # with the service environment variables automatically injected by kubernetes
  name       = "trilium-notes"
  chart      = "trilium"
  repository = "https://charts.oecis.io"
  values = [yamlencode({
    image = {
      tag = "0.60.4"
    }
    trilium = {
      extraEnv = [{
        name  = "TRILIUM_HOST"
        value = "::"
      }]
    }
    ingress = {
      enabled = true
      hosts = [{
        host = "trilium.oecis.io"
        paths = [{
          path     = "/"
          pathType = "ImplementationSpecific"
        }]
      }]
    }
  })]
}
