variable "domain" {
  type        = string
  description = "the base domain, from which all subdomains will be derived"
}


resource "helm_release" "this" {
  name       = "crowdsec"
  chart      = "crowdsec"
  repository = "https://crowdsecurity.github.io/helm-charts"

  version = "0.10.0"

  values = [
    yamlencode({
      lapi = {
        dashboard = {
          enabled = true
          ingress = {
            host    = "crowdsec.${var.domain}"
            enabled = true
          }
        }
        env = [
          {
            name  = "DISABLE_ONLINE_API"
            value = "true"
          }
        ]
      }
      agent = {
        persistentVolume = {
          config = {
            enabled = false
          }
        }
        acquisition = [
          {
            namespace = "default"
            podName   = "traefik-*"
            program   = "traefik"
          }
        ]
        env = [
          {
            name  = "COLLECTIONS"
            value = "crowdsecurity/traefik"
          },
          {
            name  = "PARSERS"
            value = "crowdsecurity/cri-logs"
          }
        ]
      }
    })
  ]
}
