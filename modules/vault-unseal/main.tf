resource "helm_release" "vault-unseal" {
  repository  = "https://charts.oecis.io"
  name        = "vault-unseal"
  chart       = "vault-unseal"
  max_history = 2
  version     = "0.6.1"
}
