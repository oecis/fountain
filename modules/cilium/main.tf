variable "host_ip" {
  description = "IPv4 address of the host"
  type        = string
}

resource "helm_release" "cilium" {
  name        = "cilium"
  repository  = "https://helm.cilium.io/"
  chart       = "cilium"
  max_history = 2
  version     = "1.14.5"

  namespace = "kube-system"

  values = [
    yamlencode({
      kubeProxyReplacement = "strict"
      k8sServiceHost       = var.host_ip
      k8sServicePort       = "6443"
      operator = {
        replicas = 1
        enabled  = true
      }
      ipv6 = {
        enabled = true
      }
      hostServices = {
        enabled = true
      }
      externalIPs = {
        enabled = true
      }
      nodePort = {
        enabled = true
        mode    = "dsr"
      }
      hostPort = {
        enabled = true
      }
      ipam = {
        mode = "kubernetes"
      }
      hubble = {
        # Currently no use case for hubble
        relay = {
          enabled = false
        }
        ui = {
          enabled = false
        }
      }
      hostFirewall = {
        enabled = true
      }
      policyEnforcementMode = "default"
      extraConfig = {
        allow-localhost = "policy"
      }
      policyAuditMode = false
    })
  ]
}

resource "kubernetes_manifest" "host_network_policy" {
  manifest = {
    apiVersion = "cilium.io/v2"
    kind       = "CiliumClusterwideNetworkPolicy"
    metadata = {
      name = "host-allowed"
    }
    spec = {
      description = "Traffic to be allowed from the outside"
      nodeSelector = {
        matchLabels = {
          # Select the our default node
          cilium-firewall = "true"
        }
      }
      egress = [
        {
          # We (currently) don't care about egress traffic
          toEntities = ["all"],
        }
      ],
      ingress = [
        {
          fromEntities = ["cluster"]
        },
        {
          toPorts = [
            {
              ports = [
                {
                  port     = "42069"
                  protocol = "TCP"
                },
                {
                  port     = "6443"
                  protocol = "TCP"
                },
                {
                  port     = "80"
                  protocol = "TCP"
                },
                {
                  port     = "443"
                  protocol = "TCP"
                },
                # DNS ports
                {
                  port     = "853"
                  protocol = "TCP"
                },
                {
                  port     = "53"
                  protocol = "UDP"
                },
                # minio ports
                {
                  port     = "35920"
                  protocol = "TCP"
                },
                {
                  port     = "35921"
                  protocol = "TCP"
                },
                # wireguard
                {
                  port     = "51820"
                  protocol = "UDP"
                },
                # teamspeak3
                {
                  port     = "9987"
                  protocol = "UDP"
                },
                {
                  port     = "10011"
                  protocol = "TCP"
                },
                {
                  port     = "30033"
                  protocol = "TCP"
                }
              ]
            }
          ]
        },
        {
          icmps = [
            {
              fields = [
                {
                  type   = 8
                  family = "IPv4"
                }
              ]
            }
          ]
        }
      ]
    }
  }
  depends_on = [helm_release.cilium]
}
