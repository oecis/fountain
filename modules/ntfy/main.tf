resource "helm_release" "ntfy" {
  chart      = "ntfy"
  repository = "https://charts.oecis.io"
  version    = "0.1.0"
  name       = "ntfy"
  values = [
    yamlencode({
      ingress = {
        enabled = true
        hosts = [{
          host = var.domain
          paths = [{
            path     = "/"
            pathType = "ImplementationSpecific"
          }]
        }]
      }
      ntfy = {
        config = {
          base-url = "https://${var.domain}"
        }
      }
    })
  ]
}
