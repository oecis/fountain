locals {
  s3 = {
    bucket   = "terraform-state"
    region   = "eu-central-1"
    endpoint = "http://localhost:35920"
    profile  = "dev"
  }
  domain      = "oecis.test"
  k8s_context = "dev"
  host = {
    ipv4 = "10.0.2.15"
    ipv6 = "2a03:4000:65:dd7::69"
  }
  nfs = {
    host = "10.98.134.2"
    path = "/"
  }
  smtp = {
    host = "10.98.134.3"
    port = 1025
    tls  = false
  }
  issuer = "cert-manager-self-signed"
  services = {
    gate = {
      routes = []
    }
  }
}
